# This Dockerfile creates a stage3 container image. 

ARG BOOTSTRAP
FROM ${BOOTSTRAP:-alpine:3.11} as builder

WORKDIR /liguros

ARG RELEASE=develop
ARG VARIANT=openssl
ARG ARCH=x86_64
ARG SUFFIX
#ARG DIST="https://build.funtoo.org/funtoo-current"
ARG DIST="https://build.liguros.net/${ARCH}"
ARG FILENAME="stage3-${VARIANT}-latest.tar.xz"
ARG BDFL_KEY="E986E8EE"
ARG BDFL_FP="E8EE"
ARG SIGNING_KEYS="11FD00FD 683A2F8A BEA87CD2 EEE54A43 62DD6D47 6B365A89"

RUN echo "Building Liguros Container image for ${ARCH} ${SUFFIX} fetching from ${DIST}" \
 && sleep 3 \
 && apk --no-cache add gnupg tar wget xz \
 && STAGE3="${DIST}/${RELEASE}/${FILENAME}" \
 && echo "STAGE3:" $STAGE3 \ 
 && wget -nv "${STAGE3}" "${STAGE3}.sum" \
# && wget -nv "${STAGE3}" "${STAGE3}.gpg" "${STAGE3}.hash.txt" \
# && gpg --list-keys \
# && echo "standard-resolver" >> ~/.gnupg/dirmngr.conf \
# && echo "honor-http-proxy" >> ~/.gnupg/dirmngr.conf \
# && echo "disable-ipv6" >> ~/.gnupg/dirmngr.conf \
# && gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys ${BDFL_KEY} ${SIGNING_KEYS} \
# && gpg --list-keys --fingerprint | grep ${BDFL_FP} | tr -d '[:space:]' | awk 'BEGIN { FS = "=" } ; { print $1 ":6:" }' | gpg --import-ownertrust \
# && gpg --verify ${FILENAME}.gpg ${FILENAME} \
 && echo "Checking hash value from hash file:" \
 && sha512sum -c ${FILENAME}.sum \
# && awk '{print $2 "  stage3-latest.tar.xz"}' ${FILENAME}.hash.txt | sha256sum -c - \
 && tar xpf ${FILENAME} --xattrs --numeric-owner \
 && sed -i -e 's/#rc_sys=""/rc_sys="docker"/g' etc/rc.conf \
 && echo 'UTC' > etc/timezone \
 && rm ${FILENAME}* \
 && rm -rf usr/src/linux-* \
 && rm -rf lib64/modules/* \ 
 && rm -rf boot/*
 
FROM scratch
 
WORKDIR /
COPY --from=builder /liguros/ /
CMD ["/bin/bash"]
